<%@ page language="java" import="java.util.*" import="model.*"
	import="model.User" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>


<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/3/w3.css">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Profile</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
	<div class="medication">
		<div class="header">
			<span style="font-size: 30px; cursor: pointer"></span>
			<h1>
				<a href="UserServlet?action=home" title="home"><img class="logo"
					src="images/pills.png" align="top"></a> MediTrack
			</h1>
		</div>

		<br>

		<c:set var="medicationList" value="${sessionScope.medicationData}" />
		<c:set var="user" value="${sessionScope.User}" />
		<table>
			<tr>
				<th>Medication</th>
				<th>Morning</th>
				<th>Afternoon</th>
				<th>Evening</th>
				<th>Night</th>
				<th></th>
			</tr>
			<c:forEach items="${medicationList}" var="medication">
				<tr>
					<td><c:out value="${medication.getMedicine()}" /></td>
					<td><c:if test="${medication.getMorning() != 0}">&#128138;</c:if></td>
					<td><c:if test="${medication.getAfternoon() != 0}">&#128138;</c:if></td>
					<td><c:if test="${medication.getEvening() != 0}">&#128138;</c:if></td>
					<td><c:if test="${medication.getNight() != 0}">&#128138;</c:if></td>
					<c:if test="${user.getRole() == 'CareTaker'}">
						<td><a
							href="MedicationServlet?action=EDIT&medicationId=${medication.getMedicationId()}">Edit</a>
						</td>
						<td><a
							href="MedicationServlet?action=DELETE&Id=${medication.getMedicationId()}">Delete</a>
						</td>
					</c:if>
				</tr>
			</c:forEach>
		</table>
		<div align="center">
			<c:if test="${user.getRole() == 'CareTaker'}">
				<a href="MedicationServlet?action=ADD">Add Event</a>
			</c:if>
		</div>
		<div class="footer">Copyright &copy; 2019</div>
	</div>
</body>
</html>