<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="java.util.Calendar" import="model.*" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="homestyle.css">
<title>Home</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>

	<!-- variable -->
	<c:set var="year" value="${sessionScope.Year}" />
	<c:set var="month" value="${sessionScope.Month}" />
	<c:set var="user" value="${sessionScope.User}" />

	<div class="home">

		<!-- header -->
		<div class="header">
			<span style="font-size: 30px; cursor: pointer"></span>
			<h1>
				<img class="logo" src="images/pills.png" align="top"></img>
				MediTrack
				<div style="float: right; padding-right: 20px;">
					<a href="LogOut" title="logout"><img class="logo"
						src="images/logout (2).png" align="top"></a>
				</div>
			</h1>
			<h3>
				<marquee style="color: white">
					Welcome
					<c:out value="${user.getFname()} ${user.getLname()}"></c:out>
				</marquee>
			</h3>
		</div>

		<!-- Row -->
		<div style="margin-top: 8%;">
			<div class="card-deck">
				<div class="card">
					<a href="UserServlet?action=profile" title="profile"><img
						src="images\user (2).png" class="card-img-top" alt="..."></a>
					<div class="card-body">
						<div style="text-align: center">
							<a href="UserServlet?action=profile" class="btn btn-primary">Profile</a>
						</div>
					</div>
				</div>
				<c:if test="${user.getRole() == 'CareTaker'}">
					<div class="card">
						<a href="FileUpload.jsp" title="Download"><img
							src="images\file (2).png" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<div style="text-align: center">
								<a href="FileUpload.jsp" class="btn btn-primary">Upload</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="FileDownload" class="btn btn-primary">Download</a>
							</div>
						</div>
					</div>
				</c:if>
				<c:if test="${user.getRole() != 'CareTaker'}">
					<div class="card">
						<a href="FileUpload.jsp" title="Upload"><img
							src="images\file (1).png" class="card-img-top" alt="..."></a>
						<div class="card-body">
							<div style="text-align: center">
								<a href="FileUpload.jsp" class="btn btn-primary">Upload</a>
							</div>
						</div>
					</div>
				</c:if>
				<div class="card">
					<a href="MedicationServlet?action=GET" title="Events"><img
						src="images\clipboard.png" class="card-img-top" alt="..."></a>
					<div class="card-body">
						<div style="text-align: center">
							<a href="MedicationServlet?action=GET" class="btn btn-primary">
								Events</a>
						</div>
					</div>
				</div>
				<div class="card">
					<a
						href="CalendarServlet?month=present&monthNumber=${month}&year=${year}"
						title="Calendar"><img src="images\calendar (1).png"
						class="card-img-top" alt="..."></a>
					<div class="card-body">
						<div style="text-align: center">
							<a
								href="CalendarServlet?month=present&monthNumber=${month}&year=${year}"
								class="btn btn-primary">Calendar</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- footer -->
		<div class="footer">Copyright &copy; 2019</div>
	</div>
</body>
</html>