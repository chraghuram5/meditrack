<%@ page language="java" import="java.util.Calendar"
	import="model.CustomCalendar" import="model.User"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="profilestyle.css">
<title>Profile</title>
</head>
<body>
	<div class="home">
		<div class="header">
			<span style="font-size: 30px; cursor: pointer"></span>
			<h1>
				<a href="UserServlet?action=home" title="home"><img class="logo"
					src="images/pills.png" align="top"></a> MediTrack
			</h1>
		</div>
		<c:set var="user" value="${sessionScope.User}" />
		<c:set var="associatedUser" value="${sessionScope.AssociatedUser}" />
		<c:set var="timings" value="${requestScope.Timings}" />
		<c:set var="status" value="${requestScope.TimingsStatus}" />
		<c:if test="${status ==  'Yes'}">
		<p style="text-align: center; color: white">Timings added successfully</p>
		</c:if>
		<div class="card-group">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title">Profile</h5>
					<div class="card-text">
						<table>
							<tr>
								<th>First Name</th>
								<td><c:out value="${user.getFname()}" /></td>
							</tr>
							<tr>
								<th>Last Name</th>
								<td><c:out value="${user.getLname()}" /></td>
							</tr>
							<tr>
								<th>E-mail</th>
								<td><c:out value="${user.getEmail()}" /></td>
							</tr>
							<tr>
								<th>Gender</th>
								<td><c:out value="${user.getGender()}" /></td>
							</tr>
							<tr>
								<th>UserName</th>
								<td><c:out value="${user.getUname()}" /></td>
							</tr>
							<tr>
								<th>Contact Number</th>
								<td><c:out value="${user.getMobile()}" /></td>
							</tr>
							<tr>
								<th>UserId</th>
								<td><c:out value="${user.getUserId()}" /></td>
							</tr>
							<tr>
								<th>Role</th>
								<td><c:out value="${user.getRole()}" /></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<c:if test="${user.getAssId() == 0}">
						<h5 class="card-title">Associated User Details:</h5>
						<div class="card-text">
							<form method="post" action="UserServlet?action=addtimings"
								name="af1" onsubmit=" return validateLoginAdmin()">
								<table>
									<tr>
										<th>Add/Edit AssociatedUser</th>
										<td><input type="number" name="associateId"></td>
									</tr>
									<tr>
										<th></th>
										<td><input type="submit" value="add"></input></td>
									</tr>
								</table>
							</form>
						</div>
					</c:if>
					<c:if test="${user.getAssId() != 0}">
						<h5 class="card-title">Associated User Details:</h5>
						<div class="card-text">
							<table>
								<tr>
									<th>Name</th>
									<td><c:out
											value="${associatedUser.getFname()} ${associatedUser.getLname()}" /></td>
								</tr>
								<tr>
									<th>UserName</th>
									<td><c:out value="${associatedUser.getUname()}" /></td>
								</tr>
								<tr>
									<th>Contact Number</th>
									<td><c:out value="${associatedUser.getMobile()}" /></td>
								</tr>
								<tr>
									<th>UserId</th>
									<td><c:out value="${associatedUser.getUserId()}" /></td>
								</tr>
							</table>
						</div>
					</c:if>
					<br>
					<h5 class="card-title">Meal Timings</h5>
					<div class="card-text">
						<c:if test="${user.getRole() == 'CareTaker'}">
							<form method="post" action="UserServlet?action=timings"
								name="af1" onsubmit=" return validateLoginAdmin()">
								<table>
									<tr>
										<th>BreakFast</th>
										<td><select name="bhours" required>
												<c:forEach var="i" begin="7" end="10">
													<c:if test="${i <= 9}">
														<option value="${i}">0${i}</option>
													</c:if>
													<c:if test="${i > 9}">
														<option value="${i}">${i}</option>
													</c:if>
												</c:forEach>
										</select> : <select name="bminutes" required>
												<c:forEach var="i" begin="0" end="45" step="15">
													<c:if test="${i <= 9}">
														<option value="${i}">0${i}</option>
													</c:if>
													<c:if test="${i > 9}">
														<option value="${i}">${i}</option>
													</c:if>
												</c:forEach>
										</select>AM</td>
									</tr>
									<tr>
										<th>Lunch</th>
										<td><select name="lhours" required>
												<option value="12">12</option>
												<c:forEach var="i" begin="1" end="3">
													<option value="${i}">0${i}</option>
												</c:forEach>
										</select> : <select name="lminutes" required>
												<c:forEach var="i" begin="0" end="45" step="15">
													<c:if test="${i <= 9}">
														<option value="${i}">0${i}</option>
													</c:if>
													<c:if test="${i > 9}">
														<option value="${i}">${i}</option>
													</c:if>
												</c:forEach>
										</select>PM</td>
									</tr>
									<tr>
										<th>Dinner</th>
										<td><select name="dhours" required>
												<c:forEach var="i" begin="6" end="9">
													<option value="${i}">0${i}</option>
												</c:forEach>
										</select> : <select name="dminutes" required>
												<c:forEach var="i" begin="0" end="45" step="15">
													<c:if test="${i <= 9}">
														<option value="${i}">0${i}</option>
													</c:if>
													<c:if test="${i > 9}">
														<option value="${i}">${i}</option>
													</c:if>
												</c:forEach>
										</select>PM</td>
									</tr>
									<tr>
										<td></td>
										<td><input type="submit" value="Add"></td>
									</tr>
								</table>
							</form>
						</c:if>
						<c:if test="${user.getRole() == 'User'}">
							<table>
								<tr>
									<th>BreakFast</th>
									<td><c:if test="${timings.getBminutes() == 0}">
											<c:out
												value="${timings.getBhours()}:0${timings.getBminutes()} AM" />
										</c:if> <c:if test="${timings.getBminutes() != 0}">
											<c:out
												value="${timings.getBhours()}:${timings.getBminutes()} AM" />
										</c:if></td>
								</tr>
								<tr>
									<th>Lunch</th>
									<td><c:if test="${timings.getLminutes() == 0}">
											<c:out
												value="${timings.getLhours()}:0${timings.getLminutes()} PM" />
										</c:if> <c:if test="${timings.getLminutes() != 0}">
											<c:out
												value="${timings.getLhours()}:${timings.getLminutes()} PM" />
										</c:if></td>
								</tr>
								<tr>
									<th>Dinner</th>
									<td><c:if test="${timings.getDminutes() == 0}">
											<c:out
												value="${timings.getDhours()}:0${timings.getDminutes()} PM" />
										</c:if> <c:if test="${timings.getDminutes() != 0}">
											<c:out
												value="${timings.getDhours()}:${timings.getDminutes()} PM" />
										</c:if></td>
								</tr>
							</table>
						</c:if>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">Copyright &copy; 2019</div>
	</div>

</body>
</html>