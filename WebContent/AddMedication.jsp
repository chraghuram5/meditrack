<%@ page language="java" import="java.util.*"
	import="model.CustomCalendar" import="model.*"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="profilestyle.css">
<title>Profile</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
	<div class="medication">
		<div class="header">
			<span style="font-size: 30px; cursor: pointer"></span>
			<h1>
				<a href="UserServlet?action=home" title="home"><img class="logo"
					src="images/pills.png" align="top"></a> MediTrack
			</h1>
		</div>

		<br>
		<c:set var="medicineList" value="${sessionScope.MedicineList}" />
		<c:set var="MedicationStatus" value="${requestScope.MedicationStatus}" />
		<c:if test="${MedicationStatus == 'added'}">
			<p style="text-align: center; color: white">Event added
				successfully</p>
		</c:if>
		<c:if test="${MedicationStatus == 'edited'}">
			<p style="text-align: center; color: white">Event edited
				successfully</p>
		</c:if>
		<div class="card-group">
			<div class="card">
				<div class="card-body">
					<div class="card-text">
						<form method="post" name="form1"
							action="MedicationServlet?action=ADD">
							<table>
								<tr>
									<th style="text-align: right">Event</th>
									<td style="text-align: left"><select name="medication"
										required>
											<c:forEach items="${medicineList}" var="medicine">
												<option value="${medicine}"><c:out
														value="${medicine}" /></option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<th style="text-align: right">Session</th>
									<td style="text-align: left"><input type="checkbox"
										name="morning" value=1>Morning<br> <input
										type="hidden" name="morning" value="0"> <input
										type="checkbox" name="afternoon" value=1>Afternoon<br>
										<input type="hidden" name="afternoon" value="0"> <input
										type="checkbox" name="evening" value=1>Evening<br>
										<input type="hidden" name="evening" value="0"> <input
										type="checkbox" name="night" value=1>Night<br></td>
									<input type="hidden" name="night" value="0">
								</tr>
								<tr>
									<th></th>
									<td style="text-align: left"><input type="submit"
										value="Add" style="border-radius: 20px"> <a
										style="padding-left: 60px" href="MedicationServlet?action=GET"
										title="Events"><img height=40px; width=40px;
											src="images\clipboard.png"></a></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<div class="card-text">
						<form method="get" name="form2" action="UserServlet">
							<table>
								<tr>
									<th style="text-align: right">Add Event</th>
									<td style="text-align: left"><input type="text"
										name="medicine" style="border-radius: 20px"></td>
								</tr>
								<tr>
									<th></th>
									<td><input type="hidden" id="custId" name="action"
										value="addmedicine"></td>
								</tr>
								<tr>
									<th></th>
									<td style="text-align: left"><input type="submit"
										value="Add" style="border-radius: 20px"></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">Copyright &copy; 2019</div>
</body>
</html>