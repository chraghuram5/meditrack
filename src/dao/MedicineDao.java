package dao;

import java.util.ArrayList;

public interface MedicineDao {
	public ArrayList<String> getMedicine();
	public void addMedicine(String medicine);
}
