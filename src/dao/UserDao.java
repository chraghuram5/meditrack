package dao;

import model.Timings;
import model.User;

public interface UserDao {
	public int addUser(User user);
	public int authenticate(String login, String password);
	public User getUser(int userId);
	public void addId(int assId, int id);
	public void addFile(int userId, String fileName);
	public String getFile(int userId);
	public void addTimings(Timings timings);
	public Timings getTimings(int userId);
}
