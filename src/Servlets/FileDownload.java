package Servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import dao.UserDaoSqlImpl;
import model.User;

/**
 * Servlet implementation class FileDownload
 */
@WebServlet("/FileDownload")
public class FileDownload extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	private static final String UPLOAD_DIR = "uploads";
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session=request.getSession();
		User user=(User)session.getAttribute("User");
		int userId=user.getUserId();
		UserDao userDao=new UserDaoSqlImpl();
		String fileName=userDao.getFile(userId);
		String userName=userDao.getUser(user.getAssId()).getUname();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String applicationPath = request.getServletContext().getRealPath("");
		applicationPath="C:\\\\Users\\\\806929\\\\Desktop\\\\meditrack\\\\WebContent";
		String uploadFilePath = applicationPath + File.separator + UPLOAD_DIR + File.separator + userName+ File.separator ;
		response.setContentType("APPLICATION/OCTET-STREAM");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		FileInputStream fileInputStream = new FileInputStream(uploadFilePath+ fileName);
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
	}

}
