package Servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MedicineDao;
import dao.MedicineDaoSqlImpl;
import dao.UserDao;
import dao.UserDaoSqlImpl;
import model.Timings;
import model.User;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/UserServlet")
public class UserServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getParameter("action");

		if (action.equals("register")) {
			// if action is register then setAttribute and forward to Registration.jsp
			request.setAttribute("RegistrationStatus", "No");
			request.getRequestDispatcher("Registration.jsp").forward(request, response);
		}

		if (action.equals("login")) {
			// if action is login then setAttribute and forward to Login.jsp
			request.setAttribute("LoginStatus", "No");
			request.getRequestDispatcher("Login.jsp").forward(request, response);
		}

		if (action.equals("profile")) {
			if (isValidSession(request, response)) {
				HttpSession session = request.getSession(false);
				User user = (User) session.getAttribute("User");
				UserDao userDao = new UserDaoSqlImpl();
				if (user.getRole().equals("User")) {
					int userId = user.getAssId();
					Timings timings = userDao.getTimings(userId);
					request.setAttribute("Timings", timings);
				}
				if (user.getRole().equals("CareTaker")) {
					request.setAttribute("TimingsStatus", "No");
					request.setAttribute("Timings", null);
				}
				request.setAttribute("TimingsStatus", "No");
				request.getRequestDispatcher("Profile.jsp").forward(request, response);
			} else
				request.getRequestDispatcher("Login.jsp").forward(request, response);
		}

		if (action.equals("addmedicine")) {
			HttpSession session = request.getSession(false);
			String medicine = request.getParameter("medicine");
			MedicineDao medicineDao = new MedicineDaoSqlImpl();
			medicineDao.addMedicine(medicine);
			ArrayList<String> medicineList = medicineDao.getMedicine();
			session.setAttribute("MedicineList", medicineList);
			request.setAttribute("MedicationStatus", "No");
			request.getRequestDispatcher("AddMedication.jsp").forward(request, response);
		}

		if (action.equals("home")) {
			if (isValidSession(request, response)) {
				request.getRequestDispatcher("Home.jsp").forward(request, response);
			} else
				request.getRequestDispatcher("Login.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		// UserDao Object creation
		UserDao userDao = new UserDaoSqlImpl();

		if (action.equals("register")) {
			// collection of all the parameters from form
			String fname = request.getParameter("fname");
			String lname = request.getParameter("lname");
			String email = request.getParameter("email");
			String gender = request.getParameter("gender");
			String uname = request.getParameter("uname");
			String mobile = request.getParameter("mobile");
			String password = request.getParameter("password");
			String role = request.getParameter("role");
			// Creation of user object with form elements
			User user = new User(fname, lname, email, gender, uname, mobile, password, role);

			// User addition to database using addUser method
			int userAddition = userDao.addUser(user);
			if (userAddition != 0)
				request.getRequestDispatcher("Login.jsp").forward(request, response);
			else {
				// Used in Registration.jsp
				request.setAttribute("RegistrationStatus", "Failed");
				request.getRequestDispatcher("Registration.jsp").forward(request, response);
			}

		}

		if (action.equals("authenticate")) {
			// Collection of form parameters
			String uname = request.getParameter("uname");
			String password = request.getParameter("password");

			// Collection of userId if user account exists
			int userId = userDao.authenticate(uname, password);
			User user = userDao.getUser(userId);
			if (userId != 0) {
				HttpSession session = request.getSession();
				// Used in BaseServlet
				session.setAttribute("Status", "Authenticated");

				/*
				 * User made available to whole session also Used in profile.jsp
				 */
				session.setAttribute("User", user);

				// If user is registered with an associate user then associate user object is
				// made available to whole session
				if (user.getAssId() != 0) {
					User associatedUser = userDao.getUser(user.getAssId());
					session.setAttribute("AssociatedUser", associatedUser);
				}

				// Medicines List required for all pages
				MedicineDao medicineDao = new MedicineDaoSqlImpl();
				ArrayList<String> medicineList = medicineDao.getMedicine();
				session.setAttribute("MedicineList", medicineList);

				// Current month and year for home page
				Calendar cal = Calendar.getInstance();
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH) + 1;
				session.setAttribute("Month", month);
				session.setAttribute("Year", year);
				request.getRequestDispatcher("Home.jsp").forward(request, response);

			} else {
				// Used in Login.jsp
				request.setAttribute("LoginStatus", "Failed");
				request.getRequestDispatcher("Login.jsp").forward(request, response);
			}
		}

		if (action.equals("update")) {
			if (isValidSession(request, response)) {
				// Collection of associatedId from form
				String id = request.getParameter("associateId");
				int associateId = Integer.parseInt(id);
				HttpSession session = request.getSession(false);
				User user = (User) session.getAttribute("User");
				int userId = user.getUserId();
				// updating associatedId with user details
				userDao.addId(associateId, userId);

				// Updated user and associatedUser details
				User associatedUser = userDao.getUser(associateId);
				user = userDao.getUser(user.getUserId());
				// Updating session attributes with updates user details
				// Used in Profile
				session.setAttribute("User", user);
				session.setAttribute("AssociatedUser", associatedUser);

				if (user.getRole().equals("User")) {
					userId = user.getAssId();
					Timings timings = userDao.getTimings(userId);
					request.setAttribute("Timings", timings);
				}
				if (user.getRole().equals("CareTaker")) {
					request.setAttribute("Timings", null);
				}

				request.getRequestDispatcher("Profile.jsp").forward(request, response);
			} else
				request.getRequestDispatcher("Login.jsp").forward(request, response);
		}

		if (action.equals("timings")) {
			// collection of all the parameters from form
			HttpSession session = request.getSession(false);
			User user = (User) session.getAttribute("User");
			int bhours = Integer.parseInt(request.getParameter("bhours"));
			int bminutes = Integer.parseInt(request.getParameter("bminutes"));
			int lhours = Integer.parseInt(request.getParameter("lhours"));
			int lminutes = Integer.parseInt(request.getParameter("lminutes"));
			int dhours = Integer.parseInt(request.getParameter("dhours"));
			int dminutes = Integer.parseInt(request.getParameter("dminutes"));
			int userId = user.getUserId();
			Timings timings = new Timings(userId, bhours, bminutes, lhours, lminutes, dhours, dminutes);
			userDao.addTimings(timings);
			request.setAttribute("Timings", null);
			request.setAttribute("TimingsStatus", "Yes");
			request.getRequestDispatcher("Profile.jsp").forward(request, response);
		}

	}

}
