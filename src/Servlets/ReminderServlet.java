package Servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ReminderDao;
import dao.ReminderDaoSqlImpl;
import dao.sendSMS;
import model.Reminder;
import model.User;

/**
 * Servlet implementation class AddReminderServlet
 */
@WebServlet("/ReminderServlet")
public class ReminderServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (isValidSession(request, response)) {
			HttpSession session = request.getSession(false);
			String action = (String) request.getParameter("action");
			ReminderDao reminderDao = new ReminderDaoSqlImpl();
			if (action.equals("EDIT")) {
				int Id = Integer.parseInt(request.getParameter("reminderId"));
				Reminder reminder = reminderDao.getReminder(Id);
				request.setAttribute("editReminder", reminder);
				request.getRequestDispatcher("EditReminder.jsp").forward(request, response);
			}
			if (action.equals("DELETE")) {
				int Id = Integer.parseInt(request.getParameter("reminderId"));
				Reminder reminder = reminderDao.getReminder(Id);
				reminderDao.deleteReminder(reminder);
				request.setAttribute("ReminderStatus", "deleted");
				request.getRequestDispatcher("AddReminder.jsp").forward(request, response);
			}
			if (action.equals("ADD")) {
				String day = request.getParameter("day1");
				String month = request.getParameter("month1");
				String year = request.getParameter("year1");
				session.setAttribute("day2", day);
				session.setAttribute("month2", month);
				session.setAttribute("year2", year);
				request.getRequestDispatcher("AddReminder.jsp").forward(request, response);
			}
		} else
			request.getRequestDispatcher("Login.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		if (isValidSession(request, response)) {
			String event = request.getParameter("event");
			int day = Integer.parseInt(request.getParameter("day"));
			int month = Integer.parseInt(request.getParameter("month"));
			int year = Integer.parseInt(request.getParameter("year"));
			int hours = Integer.parseInt(request.getParameter("hours"));
			int minutes = Integer.parseInt(request.getParameter("minutes"));
			int eday = Integer.parseInt(request.getParameter("eday"));
			int emonth = Integer.parseInt(request.getParameter("emonth"));
			int eyear = Integer.parseInt(request.getParameter("eyear"));
			int seconds = 0;
			String reminder = request.getParameter("reminder");
			String repeat = request.getParameter("repeat");
			String type = request.getParameter("type");
			String when = request.getParameter("when");
			Calendar cal = Calendar.getInstance();
			int presentYear = cal.get(Calendar.YEAR);
			int presentMonth = cal.get(Calendar.MONTH) + 1;
			int presentDay = cal.get(Calendar.DAY_OF_MONTH);
			Date todayDate = new Date(presentYear - 1900, presentMonth - 1, presentDay - 1);
			System.out.println("today's date" + todayDate.toString());
			Date startDate = new Date(year - 1900, month - 1, day);
			Date endDate = new Date(eyear - 1900, emonth - 1, eday);
			if (startDate.compareTo(todayDate) < 0) {
				request.getRequestDispatcher("AddReminder.jsp").forward(request, response);
			} else {
				User user = (User) session.getAttribute("User");
				User user1 = (User) session.getAttribute("AssociatedUser");
				int userId = user1.getUserId();
				Reminder r = new Reminder(userId, event, day, month, year, hours, minutes, reminder, repeat, type, eday,
						emonth, eyear);
				ReminderDao dao = new ReminderDaoSqlImpl();
				int count = dao.count(r);
				if (count >= 6) {
					session.setAttribute("Count", "Exceeded");
					request.getRequestDispatcher("AddReminder.jsp").forward(request, response);
				} else {
					String action = (String) request.getParameter("action");
					if (action.equals("ADD")) {
						request.setAttribute("ReminderStatus", "added");
						dao.addReminder(r);
					}
					if (action.equals("EDIT")) {
						int id = Integer.parseInt(request.getParameter("Id"));
						request.setAttribute("ReminderStatus", "edited");
						if (repeat.equals("Never")) {
							Reminder editReminder = new Reminder(userId, event, day, month, year, hours, minutes,
									reminder, repeat, type, id, eday, emonth, eyear);
							dao.editReminder(editReminder);
						} else {
							Reminder previousReminder = dao.getReminder(id);
							dao.deleteReminder(previousReminder);
							Reminder editReminder = new Reminder(userId, event, day, month, year, hours, minutes,
									reminder, repeat, type, id, eday, emonth, eyear);
							dao.addReminder(editReminder);
						}
					}
					if (repeat.equals("Every Day")) {
						while (startDate.compareTo(endDate) < 0) {
							month++;
							if (month >= 13) {
								month = month % 12;
								year++;
							}
							Reminder r1 = new Reminder(userId, event, day, month, year, hours, minutes, reminder,
									repeat, type, eday, emonth, eyear);
							if (startDate.compareTo(endDate) < 0) {
								dao.addReminder(r1);
							}
							startDate = new Date(year - 1900, month - 1, day);
						}
					}
					if (repeat.equals("Every Month")) {
						while (startDate.compareTo(endDate) < 0) {
							month++;
							if (month >= 13) {
								month = month % 12;
								year++;
							}
							Reminder r1 = new Reminder(userId, event, day, month, year, hours, minutes, reminder,
									repeat, type, eday, emonth, eyear);
							if (startDate.compareTo(endDate) < 0) {
								dao.addReminder(r1);
							}
							startDate = new Date(year - 1900, month - 1, day);
						}
					}
					if (repeat.equals("Every year")) {
						while (startDate.compareTo(endDate) < 0) {
							year++;
							Reminder r1 = new Reminder(userId, event, day, month, year, hours, minutes, reminder,
									repeat, type, eday, emonth, eyear);
							if (startDate.compareTo(endDate) < 0) {
								dao.addReminder(r1);
							}
							startDate = new Date(year - 1900, month - 1, day);
						}
					}
					if (when.equals("15")) {
						if (minutes >= 15)
							minutes = minutes - 15;
						else {
							hours--;
							minutes = minutes - 15 + 60;
						}
					}
					if (when.equals("10")) {
						if (minutes >= 10)
							minutes = minutes - 10;
						else {
							hours--;
							minutes = minutes - 10 + 60;
						}
					}
					if (when.equals("5")) {
						if (minutes >= 5)
							minutes = minutes - 5;
						else {
							hours--;
							minutes = minutes - 5 + 60;
						}
					}
					Date localTime = new Date(year - 1900, month - 1, day, hours, minutes, seconds);
					DateFormat converter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					converter.setTimeZone(TimeZone.getTimeZone("GMT"));
					String time = converter.format(localTime);
					sendSMS sms = new sendSMS();
					String mobile = user.getMobile();
					String message = sms.sendSms(time, mobile, event + reminder);
					System.out.println(time);
					request.getRequestDispatcher("AddReminder.jsp").forward(request, response);
				}
			}
		} else
			request.getRequestDispatcher("Login.jsp").forward(request, response);
	}
}