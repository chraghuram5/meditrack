
package Servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import dao.UserDao;
import dao.UserDaoSqlImpl;
import model.User;

@WebServlet("/FileUploadServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB
		maxFileSize = 1024 * 1024 * 50, // 50 MB
		maxRequestSize = 1024 * 1024 * 100) // 100 MB
public class FileUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 205242440643911308L;

	/**
	 * Directory where uploaded files will be saved, its relative to the web
	 * application directory.
	 */
	private static final String UPLOAD_DIR = "uploads";

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// gets absolute path of the web application
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("User");
		User associatedUser = (User) session.getAttribute("AssociatedUser");
		String userName = null;
		if (user.getRole().equals("User")) {
			userName = user.getUname();
			session.setAttribute("UserName", userName);
		}
		if (user.getRole().equals("CareTaker")) {
			userName = associatedUser.getUname();
			session.setAttribute("UserName", userName);
		}
		UserDao userDao = new UserDaoSqlImpl();
		String applicationPath = request.getServletContext().getRealPath("");
		// constructs path of the directory to save uploaded file
		applicationPath = "C:\\Users\\806929\\Desktop\\meditrack\\WebContent";
		String uploadFilePath = applicationPath + File.separator + UPLOAD_DIR + File.separator + userName;
		// creates the save directory if it does not exists
		File fileSaveDir = new File(uploadFilePath);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}
		System.out.println("Upload File Directory=" + fileSaveDir.getAbsolutePath());

		String fileName = null;
		// Get all the parts from request and write it to the file on server
		for (Part part : request.getParts()) {
			fileName = getFileName(part);
			part.write(uploadFilePath + File.separator + fileName);
			session.setAttribute("FileName", fileName);
			request.setAttribute("filepath", uploadFilePath + File.separator + fileName);
		}

		int userId = user.getAssId();
		if (user.getRole().equals("CareTaker")) {
			userId = user.getUserId();
		}
		userDao.addFile(userId, fileName);
		request.setAttribute("message", fileName);
		getServletContext().getRequestDispatcher("/Home.jsp").forward(request, response);
	}

	/**
	 * Utility method to get file name from HTTP header content-disposition
	 */
	private String getFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		System.out.println("content-disposition header= " + contentDisp);
		String[] tokens = contentDisp.split(";");
		for (String token : tokens) {
			if (token.trim().startsWith("filename")) {
				return token.substring(token.indexOf("=") + 2, token.length() - 1);
			}
		}
		return "";
	}
}
