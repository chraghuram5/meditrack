package model;

public class Timings {
	private int userId;
	private int bhours;
	private int bminutes;
	private int lhours;
	private int lminutes;
	private int dhours;
	private int dminutes;
	
	
	public Timings(int userId, int bhours, int bminutes, int lhours, int lminutes, int dhours, int dminutes) {
		super();
		this.userId = userId;
		this.bhours = bhours;
		this.bminutes = bminutes;
		this.lhours = lhours;
		this.lminutes = lminutes;
		this.dhours = dhours;
		this.dminutes = dminutes;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getBhours() {
		return bhours;
	}
	public void setBhours(int bhours) {
		this.bhours = bhours;
	}
	public int getBminutes() {
		return bminutes;
	}
	public void setBminutes(int bminutes) {
		this.bminutes = bminutes;
	}
	public int getLhours() {
		return lhours;
	}
	public void setLhours(int lhours) {
		this.lhours = lhours;
	}
	public int getLminutes() {
		return lminutes;
	}
	public void setLminutes(int lminutes) {
		this.lminutes = lminutes;
	}
	public int getDhours() {
		return dhours;
	}
	public void setDhours(int dhours) {
		this.dhours = dhours;
	}
	public int getDminutes() {
		return dminutes;
	}
	public void setDminutes(int dminutes) {
		this.dminutes = dminutes;
	}
	
	
}
